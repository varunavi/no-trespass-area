# No Trespass Area

The project intends to set off an alarm if any person was detected trespassing the area. Read the [blog](https://varunavi.medium.com/how-to-stop-intruders-from-trespassing-your-property-9a7e79a5a5ae) for more information about the project.

<img src="keep_out__no_trespassing.jpeg">


## Shunya Stack 
Project is built by using the Shunya stack.

-   ShunyaOS is a lightweight Operating system with built-in support for AI and IOT.
-   Shunya Stack Low Code platform to build AI, IoT and AIoT products, that allows you to rapidly create and deploy AIoT products with ease.

For more information on ShunyaOS see : http://demo.shunyaos.org


## Documentation 

#### Steps to set up the components:

1. Figuring out what component could act as the buzzer since that part was missing. Light Emmiting Diode [(LED)](https://demo.shunyaos.org/docs/si/guides/actuators/501-led) was used in place of it.
2. The part with which a person should be detected - [camera/video-source](https://demo.shunyaos.org/docs/si/guides/guides-video/) 
3. The AI with which a 'person' should be recognized and 'object' should be treated as non-interfering - [detectObjects](https://demo.shunyaos.org/docs/ai/object-detection/guides-detectObjects).

#### Compile and Debug:
1. When pipeline has passed for the commits made in the `main.cpp`, download the `build:armv7`. It would be stored in the downloads as `artifacts.zip`.
2. Next, follow these steps in the command window or VSCode - 

    Clone your repository:
    ```
    git clone {url-of-your-forked-project}
    cd {place where name-of-project is}
    ```

    Make a `build` folder:
    ```
    mkdir -p build
    ```

    Copy the downloaded artifacts.zip file into folder:
    ```
    cp -dpRv ~/Downloads/artifacts.zip
    ```
    use `cp ~/Downloads/artifacts.zip` if above doesn't work

    Unzip and remove the zipped folder
    ```
    unzip ./artifacts.zip
    rm -rf ./artifacts.zip
    ```

    Commit your repository
    ```
    git add . 
    git commit -m"Added build artifacts"
    git push
    ```

#### For running the program on raspberry pi
1. SSH access into the Raspberry Pi 4 running in Shunya Labs
    ```
    ssh shunya@144.76.165.2 -p 40222
    ```

2. Clone the project
    ```
    git clone {url-of-your-forked-project} 
    cd {project-name}
    ```

3. Run the program
    ```
    # Assuming that you are in the repo folder
    sudo cp -dpRv conf/etc/shunya/config.json /etc/shunya/config.json
    ```

    ```
    cd build/artifacts/build
    sudo ./aiotMicro
    # or directly run
    # sudo ./build/artifacts/build/aiotMicro
    ```



## Project Overview

1. Pseudo Code

    ```
    main
    {
        initialize camera;
        initialize led to off;
        while(true)
        {
            capture frame;
            convert it to json;
            classify the image;
            find out confidence percentage of the identified image;
            if(object==person & confidence>90)
                LED on;
            else
                LED off;
        }
    }
    ```

2. Flow Chart
    
    <img src="Data_Flow.png">


## Video Pitch

<img src="HOW TO STOP INTRUDERS TRESPASSING YOUR PROPERTY! (1).mp4">


## Contributing
Help us improve the project.

Ways you can help:

1.  Choose from the existing issue and work on those issues.
2.  Feel like the project could use a new feature, make an issue to discuss how it can be implemented and work on it.
3.  Find a bug create an Issue and report it.
4.  Review Issues or Merge Requests, give the developers the feedback.
5.  Fix Documentation.

## Contributors 

#### Team Lead
1. Varunavi Shettigar (@varunavi)

#### Active Contributors

1.  Darshan Nere - @darshan.nere
2.  Lokesh Kasliwal - @lkasliwal 
3.  Priyal Mohod - @priyalnm2001
